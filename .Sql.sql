create database travelworld;
use travelworld;
CREATE TABLE `cities` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `city` varchar(50) NOT NULL,
  `state_id` varchar(50) NOT NULL,
  `state_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO cities(City,State_ID,State_Name) VALUES ('Aberdeen','WA','Washington');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Abilene','TX','Texas');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Adams','WI','Wisconsin');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Adams','MN','Minnesota');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Adams','TN','Tennessee');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Adel','GA','Georgia');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Ainsworth','NE','Nebraska');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Albion','MI','Michigan');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Albion','NY','New York');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Albion','PA','Pennsylvania');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Albion','IN','Indiana');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Albion','IL','Illinois');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Albion','NE','Nebraska');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Albion','WA','Washington');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Allen','TX','Texas');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Allen','OK','Oklahoma');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Alta','IA','Iowa');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Alta','CA','California');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Alturas','FL','Florida');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Amherst','MA','Massachusetts');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Amherst','OH','Ohio');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Amherst','VA','Virginia');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Amherst','WI','Wisconsin');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Anderson','IN','Indiana');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Anderson','SC','South Carolina');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Anderson','CA','California');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Kimball','NE','Nebraska');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Kimball','TN','Tennessee');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Kimball','MN','Minnesota');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Kingstown','MD','Maryland');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Kirkwood','MO','Missouri');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Lake Holiday','IL','Illinois');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Lake Holiday','VA','Virginia');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Lakeside','FL','Florida');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Lakeside','CA','California');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Lakeside','VA','Virginia');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Lakeside','MT','Montana');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Lakeside','OR','Oregon');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Lakeside','TX','Texas');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Lakeside','OH','Ohio');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Las Vegas','NV','Nevada');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Leeds','AL','Alabama');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Leeds','UT','Utah');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Mountain City','GA','Georgia');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Mountain Home','ID','Idaho');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Mountain Home','AR','Arkansas');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Mountain Lake','MN','Minnesota');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Mountain Lakes','NJ','New Jersey');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Mountain View','CA','California');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Mountain View','NC','North Carolina');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Zillah','WA','Washington');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Zilwaukee','MI','Michigan');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Zimmerman','MN','Minnesota');
INSERT INTO cities(City,State_ID,State_Name) VALUES ('Zion','PA','Pennsylvania');


