package com.travelworld.app.trips;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import lombok.extern.slf4j.Slf4j;


@Controller
@Slf4j
public class TripController {
	private static org.slf4j.Logger logger = LoggerFactory.getLogger(TripController.class);

	List<Cities> cities;

	@Autowired
	CitiesRepository citiesRepository;

	@Autowired
	TripRepository tripRepository;

	
	@GetMapping("")
	private String viewHomePage(Model model) {
		model.addAttribute("trips", tripRepository.findAll());
		return "index";
	}


	
	@GetMapping("/addTrip")
	private String viewAddTrip(Model model) {
		cities = citiesRepository.findAll();
		logger.error("message " + cities.size());

		model.addAttribute("trip", new Trip());
		model.addAttribute("cities", cities);
		return "add_trip";
	}


	
	@PostMapping("/processAddTrip")
	private String processAddTrip(Trip trip) {
		trip.generateId();
		logger.error(" trip " + trip);
		trip.setDestination(citiesRepository.findById(Long.parseLong(trip.getDestinationId())).get());
		trip.setOrigin(citiesRepository.findById(Long.parseLong(trip.getOriginId())).get());
		tripRepository.save(trip);
		return "redirect:/";
	}
}
